package com.getmovil.com.adapter;


import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.getmovil.com.R;

import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Armando on 12/19/2016.
 */

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.MyOfficeHolder> {


    public List<HashMap<String,Object>> mData;
    private Activity mCallback = null;

    public CategoriesAdapter(List<HashMap<String,Object>> aData, Activity aCallback) {
        this.mData = aData;
        this.mCallback = aCallback;
    }

    @Override
    public MyOfficeHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_categories, parent, false);

        MyOfficeHolder tvh = new MyOfficeHolder(itemView);

        return tvh;
    }

    @Override
    public void onBindViewHolder(final MyOfficeHolder holder, final int position) {

//        HashMap<String, Object> item = mData.get(position);
//
//        holder.mId = position;
//
////        holder.textCode.setText(item.get("code").toString());
//        holder.textName.setText(item.get("start_date").toString() + " - " +
//                item.get("delivery_date").toString());
//        holder.txtCount.setText(item.get("total").toString());
//        holder.txtPrice.setText("$" +item.get("amount_sales").toString());
//
//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ((MyOfficeActivity)mCallback).onShopOpen(mData.get(holder.mId).get("id").toString());
//            }
//        });
//
//        holder.txtOrderCount.setText(item.get("order_count").toString());
//
//
////        holder.flowLayoutOrders.removeAllViews();
////        ArrayList<HashMap<String, Object>> mOrders = (ArrayList<HashMap<String, Object>>)item.get("orders");
////
////        for (int i = 0; i < mOrders.size(); i++) {
////
////            createTags(holder.flowLayoutOrders, mOrders.get(i).get("order_number").toString() + "(" +
////                            mOrders.get(i).get("count").toString() + ")",
////                    R.drawable.bg_color_flowlayout_selected, (i+1),
////                    Color.WHITE);
////        }


    }


//    /**
//     * @brief Permite crear los tags para la seleccion multiple de las provincias, dentro
//     * del flowlayout
//     * */
//    private void createTags(final FlowLayout aContainer, final String aTitle,
//                            final int aBgDrawable, int aId,
//                            int aColorText)
//    {
//        final FlowLayout.LayoutParams hashTagLayoutParams = new FlowLayout.LayoutParams(FlowLayout.LayoutParams.WRAP_CONTENT,
//                FlowLayout.LayoutParams.WRAP_CONTENT);
//        hashTagLayoutParams.setMargins(4,4,4,4);
//        final LinearLayout view = (LinearLayout) mCallback.getLayoutInflater().inflate(R.layout.lyt_flowlayout, null);
//        view.setBackgroundResource(aBgDrawable);
//        view.setLayoutParams(hashTagLayoutParams);
//        view.setTag(aId);
//
//
//        TextView title = view.findViewById(R.id.textFlowLayout);
//        title.setTextColor(aColorText);
//        title.setText(aTitle);
//        aContainer.addView(view);
//    }


    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyOfficeHolder
            extends RecyclerView.ViewHolder {

        int mId;


//        @Bind(R.id.textCode)
//        TextView textCode;
//
//        @Bind(R.id.textName)
//        TextView textName;
//
//        @Bind(R.id.txtCount)
//        TextView txtCount;
//
//        @Bind(R.id.txtPrice)
//        TextView txtPrice;
//
//        @Bind(R.id.txtOrderCount)
//        TextView txtOrderCount;

        public MyOfficeHolder(View itemView) {
            super(itemView);
//            ButterKnife.bind(this, itemView);
            mId = -1;

        }

    }



}
