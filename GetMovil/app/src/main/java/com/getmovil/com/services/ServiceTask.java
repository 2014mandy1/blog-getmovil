package com.getmovil.com.services;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Armando on 9/19/2017.
 */

public class ServiceTask extends AsyncTask<String, String, String> {

    private String reqBody;
    private String responseMessage;
    IServiceTask mCallback;
    Context mContext;
    String mError = "";
    Integer mId = - 1;
    String mUrl = "";
    Integer mMethodType = 1;
    Integer mCode;
    String mToken;


    public ServiceTask(IServiceTask aCallback, Context aContext, String reqBody,
                       Integer aId, String aUrl, Integer aMethodType, String aToken) {
        this.reqBody = reqBody;
        this.mCallback = aCallback;
        this.responseMessage = "";
        this.mContext = aContext;
        this.mId = aId;
        this.mUrl = aUrl;
        this.mMethodType = aMethodType;
        this.mCode = 500;
        this.mToken = aToken;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(60, TimeUnit.SECONDS);
        builder.readTimeout(60, TimeUnit.SECONDS);
        builder.writeTimeout(60, TimeUnit.SECONDS);

        OkHttpClient client = builder.build();

        MediaType mediaType = MediaType.parse("application/json");

        RequestBody body = null;
        if (mMethodType == 1){
            body = RequestBody.create(mediaType, this.reqBody);
        }

        Request request;
        if (mMethodType == 1)
        {
            request = new Request.Builder()
                    .url(mUrl)
                    .post(body)
                    .addHeader("content-type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .addHeader("Authorization", "bearer " + mToken)
                    .build();
        }
        else {
            request = new Request.Builder()
                    .url(mUrl)
                    .addHeader("content-type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .addHeader("Authorization", "bearer " +mToken)
                    .build();
        }

        Response response;
        try {

            if (mMethodType == 1)
            {
                Log.e("Body", request.body().toString());
            }

            Log.e("url", request.url().toString());
            Log.e("header", request.headers().toString());

            response = client.newCall(request).execute();


            this.responseMessage = onCreateMessageErrors(response);
            String result = response.body().string().toString();
            Log.e("sm23", "Mensaje Respuesta: " + response.message());
            Log.e("sm23", "Mensaje Respuesta Body: " + result);
            this.mCode  = response.code();

            if (response.code() != 200)
            {
                Log.e("por aqui", "1");
                return "";
            }
            else {

                if (response.isSuccessful()) {
                    return result;
                }
                else {
                    this.responseMessage = onCreateMessageErrors(response);
                    return  this.responseMessage;
                }

            }

        } catch (IOException e) {
            mError = e.toString();
            Log.e("Error ServiceTask", e.toString());
            return null;
        }

    }


    private String onCreateMessageErrors(Response aResponse)
    {
        String v = "";
        try {

            JSONObject obj  = new JSONObject();

            obj.put("message", aResponse.message());
            obj.put("code", aResponse.code());

            v = obj.toString();
        }
        catch (JSONException e) {
            Log.e("Error catch json", e.toString());
        }

        return v;
    }

    @Override
    protected void onPostExecute(String result) {

        if (result != null) {
            if (this.mCode == 200) {
                mCallback.result(true, result, mId);
            }
            else {
                mCallback.result(false, responseMessage, mId);
            }
        }
        else {
            mCallback.result(false, mError, mId);
        }
    }


    public interface IServiceTask {
        void result(boolean aSuccess, String aResultData, Integer aId);
    }
}
