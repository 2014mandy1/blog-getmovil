package com.getmovil.com.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Armando on 4/11/2016.
 */
public class GMPreferences {

    public static long getLongPreference(final Context context, String key, long aValue) {
        SharedPreferences preferences = context.getSharedPreferences(
                context.getPackageName(), Context.MODE_PRIVATE);
        return preferences.getLong(key, aValue);
    }

    public static void saveLongPreference(final Context context, String key, long aValue){
        SharedPreferences preferences = context.getSharedPreferences(
                context.getPackageName(), Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putLong(key, aValue);
        edit.commit();
    }

    public static int getIntPreference(final Context context, String key, int aValue) {
        SharedPreferences preferences = context.getSharedPreferences(
                context.getPackageName(), Context.MODE_PRIVATE);
        return preferences.getInt(key, aValue);
    }

    public static void saveIntPreference(final Context context, String key, int aValue){
        SharedPreferences preferences = context.getSharedPreferences(
                context.getPackageName(), Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putInt(key, aValue);
        edit.commit();
    }

    public static String getStringPreference(final Context context, String key, String aValue) {
        SharedPreferences preferences = context.getSharedPreferences(
                context.getPackageName(), Context.MODE_PRIVATE);
        return preferences.getString(key, aValue);
    }

    public static void saveStringPreference(final Context context, String key, String aValue){
        SharedPreferences preferences = context.getSharedPreferences(
                context.getPackageName(), Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putString(key, aValue);
        edit.commit();
    }

    public static boolean getBooleanPreference(final Context context, String key, boolean aValue) {
        SharedPreferences preferences = context.getSharedPreferences(
                context.getPackageName(), Context.MODE_PRIVATE);
        return preferences.getBoolean(key, aValue);
    }

    public static void saveBooleanPreference(final Context context, String key, boolean aValue){
        SharedPreferences preferences = context.getSharedPreferences(
                context.getPackageName(), Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putBoolean(key, aValue);
        edit.commit();
    }

    public static float getFloatPreference(final Context context, String key, float aValue) {
        SharedPreferences preferences = context.getSharedPreferences(
                context.getPackageName(), Context.MODE_PRIVATE);
        return preferences.getFloat(key, aValue);
    }

    public static void saveFloatPreference(final Context context, String key, float aValue){
        SharedPreferences preferences = context.getSharedPreferences(
                context.getPackageName(), Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putFloat(key, aValue);
        edit.commit();
    }

}
