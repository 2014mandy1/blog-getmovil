package com.getmovil.com.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;

import com.getmovil.com.R;
import com.getmovil.com.services.ServiceTask;
import com.getmovil.com.util.GMKeys;

import butterknife.ButterKnife;

/**
 * Created by Armando on 12/9/2017.
 */

public class SplashScreenActivity extends BaseActivity implements ServiceTask.IServiceTask{


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        ButterKnife.bind(this);


        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                new ServiceTask(SplashScreenActivity.this, getBaseContext(), "", 1, GMKeys.SERVICES_RECENT_POST, 2, "").execute();
            }
        }, 5000);


    }

    @Override
    public void onEnableConnection() {

    }

    @Override
    public void onDisableConnection() {

    }

    @Override
    public void result(boolean aSuccess, String aResultData, Integer aId) {

        Intent i = new Intent(SplashScreenActivity.this, MainActivity.class);
        if (aSuccess) {

            Log.e("result", aResultData);
            i.putExtra("recent", aResultData);
        }
        startActivity(i);
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.left_out);

    }
}
