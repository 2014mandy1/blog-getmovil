package com.getmovil.com.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.getmovil.com.R;
import com.getmovil.com.services.ServiceTask;
import com.getmovil.com.util.GMKeys;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Armando on 12/21/2017.
 */

public class CategoriesActivity extends BaseActivity implements ServiceTask.IServiceTask {

    @Bind(R.id.toolbar)
    Toolbar toolbar;


    @Bind(R.id.recycleViewCategories)
    RecyclerView recycleViewCategories;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Categorías");

        recycleViewCategories.setHasFixedSize(true);
        recycleViewCategories.setLayoutManager(new LinearLayoutManager(getBaseContext()));

        onShowDlg("Espere por favor. Cargando las categorías.");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                init();
            }
        }, 1000);
    }

    private void init() {
        new ServiceTask(CategoriesActivity.this, getBaseContext(), "", 1, GMKeys.SERVICES_CATEGORIES, 2, "").execute();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onEnableConnection() {

    }

    @Override
    public void onDisableConnection() {

    }

    @Override
    public void result(boolean aSuccess, String aResultData, Integer aId) {

        ArrayList<HashMap<String, Object>> mListCategories = new ArrayList<>();
        if (aSuccess) {

            Log.e("result", aResultData);

            try {

                JSONObject jsonObject = new JSONObject(aResultData);

                if (jsonObject.getString("status").toLowerCase().equals("ok")) {

                    JSONArray array = new JSONArray(jsonObject.getString("categories"));
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject jsonObject1 = new JSONObject(array.getString(i));

                        HashMap<String, Object> categories = new HashMap<>();
                        categories.put("id", jsonObject1.getInt("id"));
                        categories.put("slug", jsonObject1.getString("slug"));
                        categories.put("title", jsonObject1.getString("title"));
                        categories.put("description", jsonObject1.getString("description"));
                        categories.put("post_count", jsonObject1.getInt("post_count"));


                        int position = onSort(mListCategories, jsonObject1.getInt("post_count"));
                        if (position == -1) {
                            mListCategories.add(categories);
                        } else {
                            mListCategories.add(position, categories);
                        }
                    }

                    Log.e("Sort", ""+mListCategories.size());
                }

            } catch (JSONException e) {
                Log.e("Error catch json", e.toString());
            }
        }
    }

    private int onSort(ArrayList<HashMap<String, Object>> aList, int aPostCount) {

        int position = -1;

        for (int i = 0; i < aList.size(); i++) {

            if (Integer.valueOf(aList.get(i).get("post_count").toString()) < aPostCount) {
                position = i;
                i = aList.size();
            }
        }

        return position;

    }

}