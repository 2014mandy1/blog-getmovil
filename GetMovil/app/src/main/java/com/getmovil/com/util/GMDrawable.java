package com.getmovil.com.util;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

/**
 * Created by Armando on 2/24/2016.
 */
public class GMDrawable {

    public static Drawable getColorChange(Context aContext,int aIdDrawable, int aColor)
    {
        Drawable mDrawable = ContextCompat.getDrawable(aContext,aIdDrawable).mutate();
        mDrawable.setColorFilter(aColor, PorterDuff.Mode.SRC_ATOP);
        return mDrawable;
    }

    public static Drawable getColorChange(Context aContext, Drawable  aDrawable, int aColor)
    {
        aDrawable.setColorFilter(aColor, PorterDuff.Mode.SRC_ATOP);
        return aDrawable;
    }

    public static int getResourceByName(final Context context, String aName, String aType) {
        int nameResourceID = context.getResources().getIdentifier(aName, aType, context.getApplicationInfo().packageName);
        if (nameResourceID == 0) {
            throw new IllegalArgumentException("No resource string found with name " + aName);
        } else {
            return nameResourceID;
        }
    }
}
