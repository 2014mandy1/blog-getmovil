package com.getmovil.com.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.getmovil.com.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Armando on 12/21/2017.
 */

public class RecentNewsActivity extends BaseActivity {

    @Bind(R.id.recycleViewRecentNews)
    RecyclerView recycleViewRecentNews;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent_news);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Ultimas noticias");

        recycleViewRecentNews.setHasFixedSize(false);
        recycleViewRecentNews.setLayoutManager(new LinearLayoutManager(getBaseContext()));
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onEnableConnection() {

    }

    @Override
    public void onDisableConnection() {

    }
}
