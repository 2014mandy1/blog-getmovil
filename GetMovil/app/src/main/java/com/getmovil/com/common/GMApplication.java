package com.getmovil.com.common;

import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.getmovil.com.R;

import java.util.ArrayList;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Armando on 12/21/2016.
 */

public class GMApplication extends MultiDexApplication implements GMNetworkStateReceiver.NetworkStateReceiverListener {

    private GMNetworkStateReceiver networkStateReceiver;

    INetwork mCallback = null;
    IFireBase mCallBack = null;
//    Firebase mFirebase;



    public boolean isConnetedFirebase = false;
//    private String FIREBASE_URL = "https://tienda-3169a.firebaseio.com/";
//    public static  String FIREBASE_CHILD = "purchase_orders";
//    boolean mExecution = false;

    public void onAddNetworkListener(INetwork aCallback){

        mCallback = aCallback;
    }


    public GMApplication() {

    }


//    public Realm realm;

    @Override
    public void onCreate() {
        super.onCreate();

        networkStateReceiver = new GMNetworkStateReceiver(getBaseContext());
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        // initalize Calligraphy
        CalligraphyConfig.initDefault(
                new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/Roboto-Regular.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );



    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }


//    private void initFireBase()
//    {
//        Firebase.setAndroidContext(getApplicationContext());
//        mFirebase = new Firebase(FIREBASE_URL);
//
//        mFirebase.child(".info/connected").addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                boolean connected = dataSnapshot.getValue(Boolean.class);
//                if (connected) {
//                    Log.e("STATUS FIREBASE","connected");
//                    isConnetedFirebase = true;
//                } else {
//                    Log.e("STATUS FIREBASE","not connected");
//                    isConnetedFirebase = false;
//                }
//            }
//
//            @Override
//            public void onCancelled(FirebaseError firebaseError) {
//                Log.e("STATUS FIREBASE","Listener was cancelled");
//            }
//        });
//    }

    public void statusNetwork()
    {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected())
        {
            onNetworkAvailable();
        }
        else {
            onNetworkUnavailable();
        }
    }



    @Override
    public void onNetworkAvailable() {
        Log.e("GMApplication", "Network is avaliable");
    /* TODO: Your connection-oriented stuff here */


        if (mCallback != null) {
            mCallback.onAvailable();

        }
    }

    @Override
    public void onNetworkUnavailable() {
        Log.e("GMApplication", "Network is not avaliable");
    /* TODO: Your disconnection-oriented stuff here */

        if (mCallback != null) {
            mCallback.onUnavailable();
        }
    }

//    @SuppressLint("LongLogTag")
//    @Override
//    public void onComplete(FirebaseError firebaseError, Firebase firebase) {
//        Log.e("Application - onComplete", "complete");
//    }
//
//    boolean ok = false;
//
//    @Override
//    public void onDataChange(DataSnapshot dataSnapshot) {
//
////        Log.e("onDataChange", "Send");
////        if (!ok) {
////            Log.e("onDataChange", "Send ok reciver");
////            ok = true;
////            if (dataSnapshot.getValue() != null && mCallBack != null) {
////                final ArrayList<Object> list = new ArrayList<>();
////                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
////
////                    MNotification z1 = snapshot.getValue(MNotification.class);
////                    z1.setUID(snapshot.getKey());
////                    list.add(z1);
////                }
////
////
////                Handler handler = new Handler();
////                handler.postDelayed(new Runnable(){
////                    public void run(){
////
////                        if (!list.isEmpty())
////                        {
////                            if (mCallBack != null)
////                            {
////                                ok = false;
////                                mCallBack.onFireBaseDataChange(list);
////                            }
////
////
////
////                            if (!activityVisible)
////                            {
////                                Intent intent = new Intent(getBaseContext(), NotificationsActivity.class);
////                            // use System.currentTimeMillis() to have a unique ID for the pending intent
////                                PendingIntent pIntent = PendingIntent.getActivity(getBaseContext(),
////                                        (int) System.currentTimeMillis(), intent, 0);
////
////                                // build notification
////                                // the addAction re-use the same intent to keep the example short
////                                Notification n  = new Notification.Builder(getBaseContext())
////                                        .setContentTitle("Nuevas ordenes")
////                                        .setContentText("Tienes nuevas ordenes de compras!!!")
////                                        .setSmallIcon(R.drawable.ic_logo_notification)
////                                        .setContentIntent(pIntent)
////                                        .setAutoCancel(true).build();
////
////                                NotificationManager notificationManager =
////                                        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
////                                notificationManager.notify(0, n);
////                            }
////
////                        }
////
////                    }
////                }, 1000);
////
////
////            }
////        }
//
//    }
//
//    @Override
//    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//        Log.e("onChildAdded", "ok");
//    }
//
//    @Override
//    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//        Log.e("onChildChanged", "ok");
//    }
//
//    @Override
//    public void onChildRemoved(DataSnapshot dataSnapshot) {
//        Log.e("onChildRemoved", "ok");
//    }
//
//    @Override
//    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
//        Log.e("onChildMoved", "ok");
//    }
//
//    @SuppressLint("LongLogTag")
//    @Override
//    public void onCancelled(FirebaseError firebaseError) {
//
////        if (firebaseError != null)
////        {
////            Log.e("Application - onCancelled", firebaseError.getMessage());
////
////            if (mCallback != null)
////            {
////                mCallBack.onFireBaseCancelled(firebaseError.getMessage());
////            }
////
////        }
//    }
//
//
//    public void addFirebaseListener(IFireBase aCallback)
//    {
//        mCallBack = aCallback;
//    }
//
//    public void addChildContent(String aChildRef, String aContent)
//    {
//        if (!aContent.isEmpty())
//        {
//            mFirebase.child(aChildRef).push().setValue(aContent, this);
//        }
//    }
//
//
//    public void addValueListener(String aChildRef)
//    {
////        FIREBASE_CHILD = aChildRef;
////        mFirebase.child(aChildRef).addValueEventListener(this);
////        mFirebase.child(aChildRef).addChildEventListener(this);
////        mFirebase.child(aChildRef).removeEventListener(this);
//    }
//
//
//    public void addChildContent(String aChildRef, Object aContent)
//    {
//        FIREBASE_CHILD = aChildRef;
//        mFirebase.child(aChildRef).addValueEventListener(this);
////        mFirebase.child(aChildRef).addChildEventListener(this);
//        mFirebase.child(FIREBASE_CHILD).push().setValue(aContent, this);
//    }
//
//    public Firebase getFirebaseRef()
//    {
//        return this.mFirebase;
//    }


    public interface INetwork {
        void onAvailable();
        void onUnavailable();
    }

    public interface IFireBase {
        void onFireBaseDataChange(ArrayList<Object> aList);
        void onFireBaseCancelled(String aError);
        void onFireBaseComplete();
    }



    public boolean isActivityVisible() {
        return activityVisible;
    }

    public void activityResumed() {
        activityVisible = true;
    }

    public void activityPaused() {
        activityVisible = false;
    }

    private boolean activityVisible;

}
