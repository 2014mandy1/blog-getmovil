package com.getmovil.com.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.getmovil.com.common.GMApplication;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Armando on 9/6/2017.
 */

public abstract class BaseActivity extends AppCompatActivity implements GMApplication.INetwork{

    protected ProgressDialog dlg = null;
    protected  boolean mConnection = false;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((GMApplication)getApplication()).onAddNetworkListener(this);
        ((GMApplication)getApplication()).statusNetwork();

    }

    protected void onShowDlg(String aMessage)
    {
        dlg = null;
        dlg = new ProgressDialog(BaseActivity.this);
        dlg.setCancelable(false);
        dlg.setMessage(aMessage);
        dlg.show();
    }

    protected void onClose() {
        if (dlg != null) {
            dlg.dismiss();
        }
    }

    @Override
    public void onAvailable() {
        mConnection = true;
    }

    @Override
    public void onUnavailable() {
        Log.e("Result","onUnavailable");
        mConnection = false;
    }


    abstract public void onEnableConnection();
    abstract public void onDisableConnection();


}
