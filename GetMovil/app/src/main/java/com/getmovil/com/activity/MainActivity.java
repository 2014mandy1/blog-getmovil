package com.getmovil.com.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.getmovil.com.R;
import com.getmovil.com.services.ServiceTask;
import com.getmovil.com.util.GMDimensioner;
import com.getmovil.com.util.GMKeys;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, ServiceTask.IServiceTask,
    View.OnClickListener{

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.drawer_layout)
    DrawerLayout drawer;

    @Bind(R.id.nav_view)
    NavigationView navigationView;

    @Bind(R.id.litsArticles)
    LinearLayout litsArticles;

    @Bind(R.id.btnNewsMore)
    Button btnNewsMore;

    Bundle mBundle = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        btnNewsMore.setOnClickListener(this);

        mBundle = getIntent().getExtras();

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                init();
//            }
//        },1000);
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    private void init()
    {

        if (mBundle != null) {

            if (mBundle.containsKey("recent")) {
                result(true, mBundle.getString("recent"), 2);
            }
        }
        else {
            new ServiceTask(MainActivity.this, getBaseContext(), "", 1, GMKeys.SERVICES_RECENT_POST, 2, "").execute();
        }

    }

    @Override
    public void onEnableConnection() {

    }

    @Override
    public void onDisableConnection() {

    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.actionUser) {
            Toast.makeText(this, "A donde va esta funcionalidad!!!", Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {

            case R.id.nav_news: {
                Intent i = new Intent(MainActivity.this, RecentNewsActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
            break;
            case R.id.nav_categories: {
                Intent i = new Intent(MainActivity.this, CategoriesActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
            break;
            case R.id.nav_download: {
                Intent i = new Intent(MainActivity.this, DownloadActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
            break;
            case R.id.nav_tuto: {
                Intent i = new Intent(MainActivity.this, RequestTutorialActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
            break;
            case R.id.nav_apk: {
                Intent i = new Intent(MainActivity.this, RequestApkActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
            break;
            case R.id.nav_social: {
                Intent i = new Intent(MainActivity.this, SocialNetworkActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
            break;
            case R.id.nav_abouts: {
                Intent i = new Intent(MainActivity.this, AboutsActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
            break;
            case R.id.nav_options: {
                Intent i = new Intent(MainActivity.this, OptionActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
            break;
            case R.id.navCloseSession: {

            }
            break;
            case R.id.nav_feedbacks: {
                Intent i = new Intent(MainActivity.this, FeedbackActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
            break;
            case R.id.nav_profile: {
                Intent i = new Intent(MainActivity.this, MyProfileActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
            break;

        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    ArrayList<HashMap<String, Object>> mListArticles = new ArrayList<>();

    @Override
    public void result(boolean aSuccess, String aResultData, Integer aId) {

        mListArticles.clear();
        if (aSuccess) {

            Log.e("result", aResultData);

            try {

                JSONObject jsonObject = new JSONObject(aResultData);

                if (jsonObject.getString("status").toLowerCase().equals("ok")){

                    JSONArray array = new JSONArray(jsonObject.getString("posts"));
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject jsonObject1 = new JSONObject(array.getString(i));

                        HashMap<String, Object> article = new HashMap<>();
                        article.put("id", jsonObject1.getInt("id"));
                        article.put("title", jsonObject1.getString("title"));
                        article.put("title_plain", jsonObject1.getString("title_plain"));
                        article.put("content", jsonObject1.getString("content"));
                        article.put("excerpt", jsonObject1.getString("excerpt"));
                        article.put("date", jsonObject1.getString("date"));
                        article.put("modified", jsonObject1.getString("modified"));
                        article.put("comments", jsonObject1.getJSONArray("comments").length());
                        article.put("image", jsonObject1.getJSONObject("thumbnail_images").getJSONObject("full").getString("url"));
                        article.put("author", jsonObject1.getJSONObject("author").getString("name"));
                        article.put("categories", jsonObject1.getJSONArray("categories").getJSONObject(0).getString("title"));
                        article.put("url", jsonObject1.getString("url"));


                        mListArticles.add(article);
                    }

                    onCreateLayoutArticles();

                    btnNewsMore.setVisibility(View.VISIBLE);
                }
            }
            catch (JSONException e) {
                Log.e("Error catch json", e.toString());
            }

        }

    }

    private void onCreateLayoutArticles()
    {
        litsArticles.removeAllViews();

        for (int i = 0; i < mListArticles.size(); i++) {

            HashMap<String,Object> row = mListArticles.get(i);

            View layout = getLayoutInflater().inflate(R.layout.article, null, false);
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);

            int dx = GMDimensioner.dpToPx(8, getBaseContext());
            param.setMargins(dx,dx,dx,0);


            ImageView imageArticle = layout.findViewById(R.id.imageArticle);
            Picasso.with(getBaseContext()).load(row.get("image").toString()).into(imageArticle);

            TextView txtCategoryArticle = layout.findViewById(R.id.txtCategoryArticle);
            txtCategoryArticle.setText(row.get("categories").toString());

            TextView titleArticle = layout.findViewById(R.id.titleArticle);
            titleArticle.setText(GMKeys.getHtml(row.get("title").toString()));

            TextView authorArticle = layout.findViewById(R.id.authorArticle);
            authorArticle.setText(row.get("author").toString());

            TextView briefArticle = layout.findViewById(R.id.briefArticle);
            briefArticle.setText(GMKeys.getHtml(row.get("excerpt").toString()));

            TextView txtComments = layout.findViewById(R.id.txtComments);
            txtComments.setText(String.valueOf(row.get("comments").toString()));

            TextView createDate = layout.findViewById(R.id.createDate);
            createDate.setText(" - " + GMKeys.getFormateDate(row.get("modified").toString()));

            layout.setLayoutParams(param);

            layout.setTag(row.get("url").toString());
            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(view.getTag().toString()));
                    startActivity(i);
                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                }
            });

            litsArticles.addView(layout);
        }
    }

    @Override
    public void onClick(View view) {
        Log.e("onClick", "ok");
        switch (view.getId()) {
            case R.id.btnNewsMore: {



                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("https://getmovil.com/"));
                startActivity(i);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);

            }
            break;
        }
    }
}
