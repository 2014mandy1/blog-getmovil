package com.getmovil.com.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

import com.getmovil.com.R;

import butterknife.ButterKnife;

/**
 * Created by Armando on 12/9/2017.
 */

public class LoginActivity extends BaseActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

    }

    @Override
    public void onEnableConnection() {

    }

    @Override
    public void onDisableConnection() {

    }
}
