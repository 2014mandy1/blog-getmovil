package com.getmovil.com.util;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;

public class GMDimensioner {

	public static final String DIMENSIONER_WIDTH = "dimensioner.width";
	public static final String DIMENSIONER_HEIGHT = "dimensioner.height";

	public static int dpToPx(int dp, Context aContext) {
		DisplayMetrics displayMetrics = aContext.getResources().getDisplayMetrics();
		int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
		return px;
	}

	public static int pxToDp(int px, Context aContext) {
		DisplayMetrics displayMetrics = aContext.getResources().getDisplayMetrics();
		int dp = Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
		return dp;
	}


	public static void DimensionerDevice(Context aContext, AppCompatActivity App)
	{
		DisplayMetrics metrics = new DisplayMetrics();
		App.getWindowManager().getDefaultDisplay().getMetrics(metrics);
		GMPreferences.saveIntPreference(aContext, DIMENSIONER_HEIGHT, metrics.heightPixels);
		GMPreferences.saveIntPreference(aContext,DIMENSIONER_WIDTH, metrics.widthPixels);
	}


	public static  boolean isTablet(Context context) {
		TelephonyManager manager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
		if(manager.getPhoneType() == TelephonyManager.PHONE_TYPE_NONE){
			return true;
		}else{
			return false;
		}
	}

}
