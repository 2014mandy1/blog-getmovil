package com.getmovil.com.util;

import android.os.Build;
import android.text.Html;
import android.text.Spanned;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by Armando on 8/24/2017.
 */

public class GMKeys {


    public final static String NAMESPACE = "com.getmovil.com";


    public final static String URL_PRODUCTION = "https://getmovil.com/";

    public final static String SERVICES_RECENT_POST = URL_PRODUCTION + "api/get_recent_posts/";
    public final static String SERVICES_CATEGORIES = URL_PRODUCTION + "api/get_category_index/";

    public static long GetTimestamp(){
        Long actualDate = System.currentTimeMillis()/1000L;
        return Long.valueOf(actualDate);
    }


    public static Spanned getHtml(String aHtml)
    {
        Spanned value;
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.N){

            value = Html.fromHtml(aHtml,Html.FROM_HTML_MODE_LEGACY);
        }
        else {
            value = Html.fromHtml(aHtml);
        }

        return value;
    }

    public static String getFormateDate(String aDate) {

        //2017-12-19 15:15:24
        String format = "";

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd 'at' HH:mm:ss a");
        try {
            format = sdf.format(df.parse(aDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return format;
    }

}
